package com.adevento.employee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adevento.employee.dto.EmployeeDto;
import com.adevento.employee.service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	@Qualifier(value = "employeeServiceImpl")
	private EmployeeService employeeService;

	@GetMapping(value = "/employees")
	public ResponseEntity<EmployeeDto[]> getEmployees() {
		EmployeeDto[] employeeDtos = employeeService.getEmployees();
		ResponseEntity<EmployeeDto[]> responseEntity = new ResponseEntity<EmployeeDto[]>(employeeDtos, HttpStatus.OK);
		return responseEntity;
	}
	@GetMapping(value = "/loop")
	public ResponseEntity<String> getLoop() {
		String loopObj = employeeService.LoopPattern();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(loopObj, HttpStatus.OK);
		return responseEntity;
	}

}
