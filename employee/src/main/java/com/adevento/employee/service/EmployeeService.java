package com.adevento.employee.service;

import com.adevento.employee.dto.EmployeeDto;

public interface EmployeeService {

	public EmployeeDto[] getEmployees();
	public String LoopPattern();
}
