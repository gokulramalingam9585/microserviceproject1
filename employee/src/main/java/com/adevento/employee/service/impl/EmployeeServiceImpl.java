package com.adevento.employee.service.impl;

import org.springframework.stereotype.Service;

import com.adevento.employee.dto.EmployeeDto;
import com.adevento.employee.service.EmployeeService;

@Service(value = "employeeServiceImpl")
public class EmployeeServiceImpl implements EmployeeService {

	private String k;
	@Override
	public EmployeeDto[] getEmployees() {
		
		EmployeeDto employeeDto1 = createEmployeeObj(1, "Gowtham", 20, "male", "gowtham@gmail.com");
		EmployeeDto employeeDto2 = createEmployeeObj(2, "Rahul", 18, "male", "rahul@gmail.com");
		EmployeeDto employeeDto3 = createEmployeeObj(3, "Janani", 23, "female", "janani@gmail.com");
		EmployeeDto employeeDto4 = createEmployeeObj(4, "Priya", 20, "female", "priya@gmail.com");
		EmployeeDto employeeDto5 = createEmployeeObj(5, "Gokul", 22, "male", "gokul@gmail.com");
		
		EmployeeDto[] emlpoyeeDtos = new EmployeeDto[5];
		emlpoyeeDtos[0] = employeeDto1;
		emlpoyeeDtos[1] = employeeDto2;
		emlpoyeeDtos[2] = employeeDto3;
		emlpoyeeDtos[3] = employeeDto4;
		emlpoyeeDtos[4] = employeeDto5;
		return emlpoyeeDtos;
	}
	
	private EmployeeDto createEmployeeObj(int id, String name, int age,String gender, String email) {
		
		EmployeeDto employeeDto = new EmployeeDto();
		employeeDto.setId(id);
		employeeDto.setName(name);
		employeeDto.setAge(age);
		employeeDto.setGender(gender);
		employeeDto.setEmail(email);
		return employeeDto;
	}

	@Override
	public String LoopPattern() {
		String k = "";
		int i, j;
		for (i = 5; i > 0; i--) {

			System.out.print("");

			for (j = 1; j <= i; j++) {

				k += "* ";

			}

			k+="\n";

		}
		return k;
	}

}
