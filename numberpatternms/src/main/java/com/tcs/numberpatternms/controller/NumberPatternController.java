package com.tcs.numberpatternms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.numberpatternms.service.NumberPatternService;

@RestController
public class NumberPatternController {

	@Autowired
	@Qualifier(value="numberPatternServiceImpl")
	private NumberPatternService numberPatternService;
	
	@GetMapping(value="/numberpattern1")
	public ResponseEntity<String>getNumberPattern1(){
		String numberPattern1 = numberPatternService.numberPattern1();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(numberPattern1,HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value="/numberpattern2")
	public ResponseEntity<String>getNumberPattern2(){
		String numberPattern2 = numberPatternService.numberPattern2();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(numberPattern2,HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value="/numberpattern3")
	public ResponseEntity<String>getNumberPattern3(){
		String numberPattern3 = numberPatternService.numberPattern3();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(numberPattern3,HttpStatus.OK);
		return responseEntity;
	}
}
