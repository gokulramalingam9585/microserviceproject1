package com.tcs.numberpatternms.service.impl;

import org.springframework.stereotype.Service;

import com.tcs.numberpatternms.service.NumberPatternService;

@Service(value = "numberPatternServiceImpl")
public class NumberPatternSeviceImpl implements NumberPatternService {

	
	@Override
	public String numberPattern1() {
		String y="";
        for (int i = 0; i <= 5; i++) {
            for (int j = 0; j < i; j++) {
                y+=(i + " ");
            }
            y+=("\n");
        }
        return y;
	}

	@Override
	public String numberPattern2() {
		String x="";
        int n = 1;
        for (int i = 0; i <= 5; i++) {
            for (int j = 0; j < i; j++) {
                x+=(n + " ");
                n++;
            }
            x+=("\n");
        }
        return x;
		
	}

	@Override
	public String numberPattern3() {
		String x="";
        for (int i = 0; i <= 5; i++) {
            for (int j = 1; j < i; j++) {
                x+=(j + " ");
            }
            x+=("\n");
        }
        return x;
	}

}
