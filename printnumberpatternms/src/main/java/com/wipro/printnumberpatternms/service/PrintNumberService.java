package com.wipro.printnumberpatternms.service;

public interface PrintNumberService {

	public String division(int length);
	public String printEven();
	public String printOdd();
}
