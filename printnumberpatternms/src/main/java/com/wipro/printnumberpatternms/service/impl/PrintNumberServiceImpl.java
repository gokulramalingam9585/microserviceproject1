package com.wipro.printnumberpatternms.service.impl;

import org.springframework.stereotype.Service;

import com.wipro.printnumberpatternms.service.PrintNumberService;

@Service(value="printNumberServiceImpl")
public class PrintNumberServiceImpl implements PrintNumberService {

	@Override
	public String division(int length) {
		  String x="";
		  if(length>0) {
	        for (int i = 1; i <= length; i++) {
	            if (i % 11 == 0) {
	                x+=(i + " ");
	                }
	            }
		  }
	        return x;
	}

	@Override
	public String printEven() {
		String y="";
        for (int i = 1; i <= 100; i++) {
            if (i % 2 == 0) {
                y+=(i + " ");
            }
        }
        return y;
	}

	@Override
	public String printOdd() {
		 String z="";
	        for (int i = 1; i <= 100; i++) {
	            if (i % 2 != 0) {
	                z+=(i + " ");
	            }
	        }
	        return z;
	}

}
