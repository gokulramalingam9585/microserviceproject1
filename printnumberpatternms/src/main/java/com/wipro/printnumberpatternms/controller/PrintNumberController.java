package com.wipro.printnumberpatternms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.printnumberpatternms.service.PrintNumberService;

@RestController
public class PrintNumberController {
	
	@Autowired
	@Qualifier(value="printNumberServiceImpl")
	private PrintNumberService printNumberService;
	@GetMapping(value="/printnumbers")
	public ResponseEntity<String>getDivision(@RequestParam(value="length")int length){
		String printnumber1  = printNumberService.division(length);
		ResponseEntity<String> reaResponseEntity = new ResponseEntity<String>(printnumber1,HttpStatus.OK);
		return reaResponseEntity;
	}
	
	@GetMapping(value="/evennumbers")
	public ResponseEntity<String>getEvenNumber(){
		String printEvenNumber  = printNumberService.printEven();
		ResponseEntity<String> reaResponseEntity = new ResponseEntity<String>(printEvenNumber,HttpStatus.OK);
		return reaResponseEntity;
	}
	
	@GetMapping(value="/oddnumbers")
	public ResponseEntity<String>getOddNumber(){
		String printOddNumber  = printNumberService.printOdd();
		ResponseEntity<String> reaResponseEntity = new ResponseEntity<String>(printOddNumber,HttpStatus.OK);
		return reaResponseEntity;
	}

}
