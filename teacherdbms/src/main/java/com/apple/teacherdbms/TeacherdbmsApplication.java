package com.apple.teacherdbms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeacherdbmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeacherdbmsApplication.class, args);
	}

}
