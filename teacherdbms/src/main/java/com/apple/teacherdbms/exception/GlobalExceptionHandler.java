package com.apple.teacherdbms.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.apple.teacherdbms.dto.ErrorMessageDto;

public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler({InvalidInputexception.class})
	public ResponseEntity<ErrorMessageDto> handleInvalidInputEx(InvalidInputexception iEx) {
		String errorMessage = iEx.getErrorMessage();
		String errorCode = iEx.getErrorCode();
		ErrorMessageDto errorMessageDTO = new ErrorMessageDto(errorMessage, errorCode);
		ResponseEntity<ErrorMessageDto> responseEntity = new ResponseEntity<ErrorMessageDto>(errorMessageDTO, HttpStatus.NOT_FOUND);
		return responseEntity;
	}
}
