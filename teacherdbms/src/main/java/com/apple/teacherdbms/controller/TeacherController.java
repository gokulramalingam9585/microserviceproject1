package com.apple.teacherdbms.controller;

import java.util.Map;
import java.util.SortedSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.apple.teacherdbms.dto.TeachersDto;
import com.apple.teacherdbms.service.TeacherService;

@RestController
public class TeacherController {

	@Autowired
	@Qualifier(value = "teacherServiceImpl")

	private TeacherService teacherService;

	@GetMapping(value = "/teachers")
	public ResponseEntity<SortedSet<TeachersDto>> getTeachers() {
		SortedSet<TeachersDto> teachersDto = teacherService.getTeachers();
		ResponseEntity<SortedSet<TeachersDto>> responseEntity = new ResponseEntity<SortedSet<TeachersDto>>(teachersDto,
				HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(value = "/getheight/{heightval}")
	public ResponseEntity<Map<Integer, SortedSet<TeachersDto>>> getTeachersHeight(
			@PathVariable(value = "heightval") int heightval) {
		Map<Integer, SortedSet<TeachersDto>> teachersDto = teacherService.getTeachersHeight(heightval);
		ResponseEntity<Map<Integer, SortedSet<TeachersDto>>> responseEntity = new ResponseEntity<Map<Integer, SortedSet<TeachersDto>>>(
				teachersDto, HttpStatus.OK);
		return responseEntity;
	}

	@GetMapping(value = "/getsalary/{salaryval}")
	public ResponseEntity<Map<Long, SortedSet<TeachersDto>>> getTeachersSalary(
			@PathVariable(value = "salaryval") long salaryval) {
		Map<Long, SortedSet<TeachersDto>> teachersDto = teacherService.getTeachersSalary(salaryval);
		ResponseEntity<Map<Long, SortedSet<TeachersDto>>> responseEntity = new ResponseEntity<Map<Long, SortedSet<TeachersDto>>>(
				teachersDto, HttpStatus.OK);
		return responseEntity;
	}

	@PostMapping(value = "/teachers")
	public ResponseEntity<Void> createTeachers(@RequestBody TeachersDto teachersDto) {
		teacherService.CreateTeachers(teachersDto);
		ResponseEntity<Void> responseEntity = new ResponseEntity<Void>(HttpStatus.CREATED);
		return responseEntity;
	}
}
