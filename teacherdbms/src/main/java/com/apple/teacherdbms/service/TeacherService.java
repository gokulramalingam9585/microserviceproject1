package com.apple.teacherdbms.service;

import java.util.Map;
import java.util.SortedSet;

import com.apple.teacherdbms.dto.TeachersDto;

public interface TeacherService {

	public SortedSet<TeachersDto> getTeachers();

	public Map<Integer, SortedSet<TeachersDto>> getTeachersHeight(int heightval);

	public Map<Long, SortedSet<TeachersDto>> getTeachersSalary(long salary);

	public void CreateTeachers(TeachersDto teachersDto);

}
