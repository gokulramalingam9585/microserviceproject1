package com.apple.teacherdbms.dto;

public class TeachersDto implements Comparable<TeachersDto> {

	private int id;
	private String name;
	private int age;
	private int height;
	private long salary;
	private String email;

//	public TeachersDto() {
//		super();
//		this.id = id;
//		this.name = name;
//		this.age = age;
//		this.height = height;
//		this.salary = salary;
//		this.email = email;
//	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public long getSalary() {
		return salary;
	}

	public void setSalary(long salary) {
		this.salary = salary;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {

		return "TeachersDto [id=" + id + ", name=" + name + ", age=" + age + ", height=" + height + ", salary=" + salary
				+ ", email=" + email + "]";
	}

	@Override
	public int compareTo(TeachersDto teachersDto) {

		 int result=this.email.compareTo(teachersDto.getEmail());
		 return result;
	}
}
