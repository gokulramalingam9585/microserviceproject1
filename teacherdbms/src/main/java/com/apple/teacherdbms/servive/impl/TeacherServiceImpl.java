package com.apple.teacherdbms.servive.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.springframework.stereotype.Service;

import com.apple.teacherdbms.dto.TeachersDto;
import com.apple.teacherdbms.service.TeacherService;

@Service(value = "teacherServiceImpl")
public class TeacherServiceImpl implements TeacherService {

	@Override
	public SortedSet<TeachersDto> getTeachers() {
		SortedSet<TeachersDto> teacherSet = new TreeSet<>();
		try {
			Connection conn = getConnection();
			// Create Statement
			Statement stmt = conn.createStatement();
			// Execute Query
			ResultSet rs = stmt.executeQuery("select * from teachers_info");
			// iterate the Resultset
			// int i = 0;
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int age = rs.getInt("age");
				int height = rs.getInt("height");
				long salary = rs.getLong("salary");
				String email = rs.getString("email");
				TeachersDto teachersDto = createTeacherObj(id, name, age, height, salary, email);
				// emlpoyeeDtos[] = employeeDto;
				teacherSet.add(teachersDto);
			}
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return teacherSet;
	}

	@Override
	public Map<Integer, SortedSet<TeachersDto>> getTeachersHeight(int heightval) {

		
//		if(heightval >= 0) {
//			TeachersDto teachersDto = null;
			Map<Integer, SortedSet<TeachersDto>> map = new HashMap<>();
		try {
			Connection conn = getConnection();
			// Create Statement
			Statement stmt = conn.createStatement();
			// Execute Query
			ResultSet rs = stmt.executeQuery("select * from teachers_info where height=" + heightval);
			// iterate the Resultset
			// int i = 0;
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int age = rs.getInt("age");
				int height = rs.getInt("height");
				long salary = rs.getLong("salary");
				String email = rs.getString("email");
				TeachersDto teachersDto1 = createTeacherObj(id, name, age, height, salary, email);
				addTeacherMap(map, teachersDto1);
			}
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
//		if (map == null) {
//			throw new InvalidInputexception("No Matching records found. Please check the height", "TDBMS007");
//		}
		return map;
//		}else {
//			throw new InvalidInputexception("Your input is invalid", "TRMS101");
//		}
		
	}

	public void addTeacherMap(Map<Integer, SortedSet<TeachersDto>> mapofTeacher, TeachersDto teachersDto) {
		int height = teachersDto.getHeight();
		if (mapofTeacher.containsKey(height)) {
			SortedSet<TeachersDto> newValList = mapofTeacher.get(height);
			newValList.add(teachersDto);
		} else {
			SortedSet<TeachersDto> studentList = new TreeSet<>();
			studentList.add(teachersDto);
			mapofTeacher.put(height, studentList);
		}
	}

	private Connection getConnection() throws ClassNotFoundException, SQLException {
		// Load the Driver
		Class.forName("com.mysql.jdbc.Driver");
		// Establish the connection
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/teachers_details", "root", "root");
		return conn;
	}

	private TeachersDto createTeacherObj(int id, String name, int age, int height, long salary, String email) {
		TeachersDto teachersDto = new TeachersDto();
		teachersDto.setId(id);
		teachersDto.setName(name);
		teachersDto.setAge(age);
		teachersDto.setHeight(height);
		teachersDto.setSalary(salary);
		teachersDto.setEmail(email);
		return teachersDto;
	}

	@Override
	public Map<Long, SortedSet<TeachersDto>> getTeachersSalary(long salaryval) {
		Map<Long, SortedSet<TeachersDto>> map = new HashMap<>();
		try {
			Connection conn = getConnection();
			// Create Statement
			Statement stmt = conn.createStatement();
			// Execute Query
			ResultSet rs = stmt.executeQuery("select * from teachers_info where salary=" + salaryval);
			// iterate the Resultset
			// int i = 0;
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int age = rs.getInt("age");
				int height = rs.getInt("height");
				long salary = rs.getLong("salary");
				String email = rs.getString("email");
				;
				TeachersDto teachersDto = createTeacherObj(id, name, age, height, salary, email);
				addTeacherMap1(map, teachersDto);
			}
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return map;
	}

	public void addTeacherMap1(Map<Long, SortedSet<TeachersDto>> mapofTeacher, TeachersDto teachersDto) {
		Long salary = teachersDto.getSalary();
		if (mapofTeacher.containsKey(salary)) {
			SortedSet<TeachersDto> newValList = mapofTeacher.get(salary);
			newValList.add(teachersDto);
		} else {
			SortedSet<TeachersDto> studentList = new TreeSet<>();
			studentList.add(teachersDto);
			mapofTeacher.put(salary, studentList);
		}
	}

	@Override
	public void CreateTeachers(TeachersDto teacherDto) {
		  try {
	            //Load the Driver
	            Connection conn = getConnection();
	            //Create Statement
	            PreparedStatement pStmt = conn.prepareStatement
	                    ("insert into teachers_info (name, age, height, salary, email) values (?, ?, ?, ?, ?)");
	            pStmt.setString(1,teacherDto.getName());
	            pStmt.setInt(2, teacherDto.getAge());
	            pStmt.setInt(3, teacherDto.getHeight());
	            pStmt.setLong(4, teacherDto.getSalary());
	            pStmt.setString(5, teacherDto.getEmail());
	            //Execute Query
	            boolean insertStatus = pStmt.execute();
	            System.out.println("insertStatus--->"+insertStatus);
//	            int i = 0;
	        } 
	        catch (SQLException sqlEx) {
	            sqlEx.printStackTrace();
	        } catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        }
	}
	
}
