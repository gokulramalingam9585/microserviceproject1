package com.google.starpatternms.service.impl;

import org.springframework.stereotype.Service;

import com.google.starpatternms.service.StarPatternService;

@Service(value = "starPatternServiceImpl")
public class StarPatternServiceImpl implements StarPatternService {

	@Override
	public String starPattern1() {
		String x  = "";
		int i, j;
		for (i = 0; i <=5 ; i++) {

			System.out.print("");

			for (j = 1; j <= i; j++) {

				x+=("* ");

			}

				x+="\n";

		}
		return x;
	}

	@Override
	public String starPattern2() {
		String x  = "";
		int i, j;
		for (i = 5; i >0 ; i--) {

			System.out.print("");

			for (j = 1; j <= i; j++) {

				x+=("* ");

			}

				x+="\n";

		}
		return x;
	}

	@Override
	public String starPattern3() {
		String x = "";
		int i, j;
		for (i = 0; i < 5; i++) {
			for (j = 0; j <= i; j++) {
				x+="* ";
			}
			x+="\n";
		}
		for (i = 4; i > 0; i--) {
			for (j = 1; j <= i; j++) {
				x+="* ";
			}
			x+="\n";

		}
		return x;
	}

	@Override
	public String starPattern4() {
		 String x="";
	        int i, j, k;
	        for (i = 0; i < 6; i++) {
	            for (j = 6 - i; j > 1; j--) {
	                x +=(" ");
	            }
	            for (k = 0; k <= i; k++) {
	                x +=("* ");
	            }
	            x +=("\n");
	        }
	        return x;
	}

	@Override
	public String starPattern5() {
		 String x="";
	        for (int i = 1; i <= 5; i++) {
	            for (int j = 0; j <= i; j++) {
	                x+=(" ");
	            }
	            for (int k = 0; k < 6 - i; k++) {
	                x+=("* ");
	            }
	            x+=("\n");
	        }
	        return x;
	}

	@Override
	public String starPattern6() {
		String x="";
        for (int i = 1; i <= 5; i++) {
            for (int j = 5; j > i; j--) {
                x+=(" ");
            }
            for (int k = 1; k <= (i * 2) - 1; k++) {
                x+=("*");
            }
            x+=("\n");
        }
        for (int i = 5 - 1; i >= 1; i--) {
            for (int j = 5 - 1; j >= i; j--) {
                x+=(" ");
            }
            for (int k = 1; k <= (i * 2) - 1; k++) {
                x+=("*");
            }

            x+=("\n");
        }
        return x;
	}

}
