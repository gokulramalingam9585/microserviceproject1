package com.google.starpatternms.service;

public interface StarPatternService {

	public String  starPattern1();
	public String starPattern2();
	public String starPattern3();
	public String starPattern4();
	public String starPattern5();
	public String starPattern6();
}
