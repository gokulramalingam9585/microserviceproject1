package com.google.starpatternms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.starpatternms.service.StarPatternService;

@RestController
public class StarPatternController {

	@Autowired
	@Qualifier(value = "starPatternServiceImpl")
	private StarPatternService starPatternService;
	
	@GetMapping(value = "/loop1")
	public ResponseEntity<String>getStarPattern1(){
		String loopPattern1 = starPatternService.starPattern1();
		ResponseEntity<String>reponseEntity =  new ResponseEntity<String>(loopPattern1, HttpStatus.OK);
		return reponseEntity;
		
	}
	
	@GetMapping(value = "/loop2")
	public ResponseEntity<String> getStarPattern2(){
		String loopPattern2 = starPatternService.starPattern2();
		ResponseEntity<String> reponseEntity =  new ResponseEntity<String>(loopPattern2, HttpStatus.OK);
		return reponseEntity;
		
	}
	
	@GetMapping(value = "/loop3")
	public ResponseEntity<String> getStarPattern3(){
		String loopPattern3 = starPatternService.starPattern3();
		ResponseEntity<String> reponseEntity =  new ResponseEntity<String>(loopPattern3, HttpStatus.OK);
		return reponseEntity;
		
	}
	
	@GetMapping(value = "/loop4")
	public ResponseEntity<String> getStarPattern4(){
		String loopPattern4 = starPatternService.starPattern4();
		ResponseEntity<String> reponseEntity =  new ResponseEntity<String>(loopPattern4, HttpStatus.OK);
		return reponseEntity;
		
	}
	
	@GetMapping(value = "/loop5")
	public ResponseEntity<String> getStarPattern5(){
		String loopPattern5 = starPatternService.starPattern5();
		ResponseEntity<String> reponseEntity =  new ResponseEntity<String>(loopPattern5, HttpStatus.OK);
		return reponseEntity;
		
	}
	
	@GetMapping(value = "/loop6")
	public ResponseEntity<String> getStarPattern6(){
		String loopPattern6 = starPatternService.starPattern6();
		ResponseEntity<String> reponseEntity =  new ResponseEntity<String>(loopPattern6, HttpStatus.OK);
		return reponseEntity;
		
	}
}
