package com.google.starpatternms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarpatternmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(StarpatternmsApplication.class, args);
	}

}
